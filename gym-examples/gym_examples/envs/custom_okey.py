import numpy as np
import enum
from ordered_enum import ValueOrderedEnum
from itertools import combinations

import gymnasium as gym
from gymnasium import spaces

class CustomOkeyEnv(gym.Env):
    metadata = {'render_modes': ['text', 'none']}
    
    """
        hand_length: the number of cards in the player's hand
        points_goal: the number of points the player needs to reach to win the game
        lookup: if True, the player can see the last card in the deck, it adds it as the last element in the observation
    """
    def __init__(self, hand_length, points_goal=None, render_mode=None, lookup=False):
        self.lookup = lookup
        self.player_hand = CardDeck(empty=True)
        self.hand_length = hand_length
        
        self.deck = CardDeck()
        self.deck.shuffle()
    
        self.score = 0
        self.steps = 0
        self.terminated = False
        
        self.rs = RewardSystem()
        self.ps = PointsSystem(render_mode == 'text')
        
        self.points_goal = points_goal
        
        self.action_space = self._create_action_space(hand_length)
        self.observation_space = self._create_observation_space(hand_length) # player's hand, first row is the value of the card, second row is the suit of the card

        self._draw_player_hand(self.deck)
        
        assert render_mode is None or render_mode in self.metadata["render_modes"]
        self.render_mode = render_mode

    def step(self, action):
        action_type, action = self._action_index_to_action(action)
        
        reward = 0
        if action_type == self.ActionType.CHECK:
            reward = self._check(action)
        else:
            reward = self._swap(action)
            
        obs = self._get_observation()
        info = self._get_info()
        
        self.terminated = False
        if self.points_goal is not None and self.score >= self.points_goal:
            if self.render_mode == 'text':
                print(f'\n\nGame over! The goal of {self.points_goal} has been reached!')
            
            # self.terminated = True

        self.steps += 1

        if self.steps > 24:
            self.terminated = True
        
        if self.render_mode is not None:
            self.render()
        
        return obs, reward, self.terminated, False, info

    def reset(self, seed=None, options=None):
        super().reset(seed=seed)
        self.score = 0
        self.steps = 0
        self.deck = CardDeck()
        self.deck.shuffle()
        self.player_hand = CardDeck(empty=True)
        self._draw_player_hand(self.deck)
        obs = self._get_observation()
        info = self._get_info()
        if self.render_mode == 'text':
            print('Resetting...')
            self.render()
        return obs, info

    def render(self):
        if self.render_mode is None:
            return
        if self.render_mode == 'text':
            self._visualize_state()
        
    def _get_observation(self): 
        if len(self.player_hand) == 0:
            self.terminated = True

        hand_array = CardDeck.array_from_deck(self.player_hand)
        if self.lookup:
            hand_array = np.vstack((hand_array, self.deck.lookup_last().to_array()))
        
        hand_obs = np.zeros((8, 3), dtype=np.uint8)
        for val, suit in hand_array:
            hand_obs[val, suit] = 1

        deck_obs = np.zeros((8, 3), dtype=np.uint8)
        for val, suit in CardDeck.array_from_deck(self.deck):
            deck_obs[val, suit] = 1

        scores_obs = np.zeros(self.nr_moves, dtype=np.int16)
        for i in range(self.nr_moves):
            action_type, action = self._action_index_to_action(i)

            if action_type == self.ActionType.CHECK:
                scores_obs[i] = self._check_reward(action)
            else:
                scores_obs[i] = self._swap_reward(action)

        return {"hand": hand_obs, "deck": deck_obs, "scores": scores_obs}
        # return {"deck": deck_obs}
    
    def _get_info(self):
        return {'score': self.score}
    
    def _draw_player_hand(self, deck):
        for _ in range(self.hand_length):
            self.player_hand.append(deck.draw())
    
    def _create_action_space(self, hand_length):
        k = 3
        self.check_actions = list(combinations(range(hand_length), k)) 

        self.nr_moves = len(self.check_actions) + hand_length

        return spaces.Discrete(self.nr_moves) 
    
    def _create_observation_space(self, hand_length):
        hand_obs_length = hand_length
        if(self.lookup):
            hand_obs_length += 1

        # hand_space = spaces.MultiDiscrete([[8, 3] for _ in range(hand_obs_length)])
        hand_space = spaces.Box(low=0, high=1, shape=(8, 3), dtype=np.uint8)

        deck_space = spaces.Box(low=0, high=1, shape=(8, 3), dtype=np.uint8)

        scores_space = spaces.Box(low=-np.inf, high=np.inf, shape=(self.nr_moves,), dtype=np.int16)

        return spaces.Dict({"hand": hand_space, "deck": deck_space, "scores": scores_space})
        # return spaces.Dict({"deck": deck_space})
    
    class ActionType(enum.Enum):
        CHECK = 0
        SWAP = 1
    
    def _action_index_to_action(self, action_index) -> tuple[ActionType, int]:
        if action_index < len(self.check_actions):
            return self.ActionType.CHECK, self.check_actions[action_index]
        else:
            return self.ActionType.SWAP, action_index - len(self.check_actions)
    
    def _check_reward(self, action):
        if any(i >= len(self.player_hand) for i in action):
            return 0

        cards = [self.player_hand[i] for i in action]
        
        action_results = {}
        action_results['sequence'] = self._check_sequence(cards)
        action_results['eq_value'] = self._check_eq_value(cards)
        action_results['eq_suit'] = self._check_eq_suit(cards)
        
        reward = self.rs.calculate_reward(action_results, cards)

        return reward
        
        
    def _check(self, action):
        if any(i >= len(self.player_hand) for i in action):
            return 0

        cards = [self.player_hand[i] for i in action]
        
        if self.render_mode == 'text':
            print(f'Checking cards: {cards}')
        
        action_results = {}
        action_results['sequence'] = self._check_sequence(cards)
        action_results['eq_value'] = self._check_eq_value(cards)
        action_results['eq_suit'] = self._check_eq_suit(cards)
        
        reward = self.rs.calculate_reward(action_results, cards)
        self.score += self.ps.calculate_points(action_results, cards)
        
        if action_results['sequence'] or action_results['eq_value']:
            for i in action:
                drawn_card = self.deck.draw()
                self.player_hand[i] = drawn_card
                if len(self.deck) == 0:
                    self._on_empty_deck()
        
        for i in range(len(self.player_hand)-1, -1, -1):
            if self.player_hand[i] is None:
                del self.player_hand[i]
                
        return reward
    
    def _swap_reward(self, action):
        if action >= len(self.player_hand):
            return 0

        return self.rs.get_draw_card_reward()
    
    def _swap(self, action):
        if action >= len(self.player_hand):
            return 0

        old_card = self.player_hand[action]

        drawn_card = self.deck.draw()
        if drawn_card is not None:
            self.player_hand[action] = drawn_card
        else:
            del self.player_hand[action]

        reward = self.rs.get_draw_card_reward()
        
        if self.render_mode == 'text':
            print(f'Swapping card: {old_card} with {self.player_hand[action]}')
        
        if len(self.deck) == 0:
            self._on_empty_deck()
        return reward
    
    def _check_sequence(self, cards):
        sorted_cards = sorted(cards, key=lambda card: card.value.value)
        return all(sorted_cards[i].value.value + 1 == sorted_cards[i+1].value.value for i in range(len(sorted_cards) - 1))
    
    def _check_eq_value(self, cards):
        return len(set([card.value for card in cards])) == 1
    
    def _check_eq_suit(self, cards):
        return len(set([card.suit for card in cards])) == 1
    
    def _visualize_state(self):
        print('\nPlayer hand:')
        self.player_hand.visualize()
        print(f'Score: {self.score}')
        if self.lookup:
            print(f'Last card in deck: {self.deck.lookup_last()}')
        
    def _on_empty_deck(self): 
        if self.render_mode == 'text':
            print('Deck is empty!!! \n')
        # Could be changed to a different behavior
        # self.reset() # reset the game when the deck is empty
        self.terminated = True

class Card():
    def __init__(self, suit, value):
        self.suit = suit
        self.value = value

    def __repr__(self):
        return f"{self.value.name} of {self.suit.name}"

    def __str__(self):
        return self.__repr__()
    
    def to_array(self):
        return np.array([self.value.value, self.suit.value])
    
@enum.unique
class CardSuit(ValueOrderedEnum):
    RED = 0
    YELLOW = 1
    BLUE = 2
    
@enum.unique
class CardValue(ValueOrderedEnum):
    ONE = 0
    TWO = 1
    THREE = 2
    FOUR = 3
    FIVE = 4
    SIX = 5
    SEVEN = 6
    EIGHT = 7
    
class CardDeck():
    def __init__(self, empty=False):
        self.cards = []
        if not empty:
            self._generate_deck()

    def _generate_deck(self):
        self.cards = []
        for suit in CardSuit:
            for value in CardValue:
                self.cards.append(Card(suit, value))
                
    def shuffle(self):
        np.random.shuffle(self.cards)
        
    def draw(self):
        if len(self.cards) == 0:
            return None
        return self.cards.pop()
    
    def append(self, card):
        self.cards.append(card)
        
    def lookup_card(self, index):
        return self.cards[index]
    
    def lookup_last(self):
        if len(self.cards) == 0:
            return None
        return self.cards[-1]
        
    @staticmethod
    def deck_from_array(arr):
        deck = CardDeck(empty=True)
        for card in arr:
            deck.append(Card(CardSuit(card[1]), CardValue(card[0])))
        return deck
    
    @staticmethod
    def array_from_deck(deck):
        return np.array([[card.value.value, card.suit.value] for card in deck.cards])
    
    
    def __len__(self):
        return len(self.cards)
    
    def __repr__(self):
        return f"CardDeck({len(self.cards)})"
    
    def __str__(self):
        return self.__repr__()
    
    def __len__(self):
        return len(self.cards)
    
    def __getitem__(self, key):
        return self.cards[key]
    
    def __setitem__(self, key, value):
        self.cards[key] = value
    
    def __delitem__(self, key):
        del self.cards[key]
  
    def visualize(self):
        s = 'Deck:\n\t'
        for card in self.cards:
            s += f'{str(card)},  '
        print(s)

class RewardSystem():
    def __init__(self):
        self.invalid_action_penalty = -1
        self.draw_card_reward = 0
        self.equal_value_reward = 20
        self.sequence_reward = 10
        self.sequence_same_suit_reward = 50
           
    """
        action_results: 
            - sequence: player's hand is a sequence
            - eq_value: player's hand has cards of equal value
            - eq_suite: player's hand has cards of equal suit
        cards: the cards that the player has checked, could be used to calculate the reward, not implemented yet
    """
    def calculate_reward(self, action_results, cards=None):
        if not action_results['sequence'] and not action_results['eq_value']:
            return self.invalid_action_penalty
        
        if action_results['sequence'] and action_results['eq_suit']:
            return self.sequence_same_suit_reward
        
        if action_results['sequence']:
            return self.sequence_reward
        
        if action_results['eq_value']:
            return self.equal_value_reward
        
        raise Exception('Invalid action results ' + str(action_results))

        
    def get_draw_card_reward(self):
        return self.draw_card_reward
    
class PointsSystem():
    def __init__(self, text_mode=False):
        self.equal_value_points = 20
        self.sequence_points = 10
        self.sequence_same_suit_points = 50
        
        self.text_mode = text_mode
           
    """
        action_results: 
            - sequence: player's hand is a sequence
            - eq_value: player's hand has cards of equal value
            - eq_suite: player's hand has cards of equal suit
        cards: the cards that the player has checked, could be used to calculate the points, not implemented yet
    """
    def calculate_points(self, action_results, cards=None):
        if(not action_results['sequence'] and not action_results['eq_value']):
            if self.text_mode:
                print('No matching cards')
                
            return 0
        
        if(action_results['sequence'] and action_results['eq_suit']):
            if self.text_mode:
                print(f'Sequence of same suit!!! {self.sequence_same_suit_points} points')
            
            return self.sequence_same_suit_points
        
        if(action_results['sequence']):
            if self.text_mode:
                print(f'Sequence!, {self.sequence_points} points')
            
            return self.sequence_points
        
        if(action_results['eq_value']):
            if self.text_mode:
                print(f'Equal value!, {self.equal_value_points} points')
            
            return self.equal_value_points
        
        raise Exception('Invalid action results ' + str(action_results))
        
