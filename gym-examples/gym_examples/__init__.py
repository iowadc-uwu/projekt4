from gymnasium.envs.registration import register

register(
    id="gym_examples/GridWorld-v0",
    entry_point="gym_examples.envs:GridWorldEnv",
)

# Register our custom environment
register(
    id="gym_examples/Okey-v0",
    entry_point="gym_examples.envs:CustomOkeyEnv",
)
