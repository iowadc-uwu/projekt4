import gym_examples
import numpy as np
import gymnasium as gym

def train_qlearning(env, num_episodes, alpha, gamma, epsilon, verbose=False):
    # observation space and action space are multi-discrete
    q_table = np.zeros(env.observation_space.shape + (env.action_space.n,))
    rewards_per_episode = []

    for episode in range(num_episodes):
        total_reward = 0
        state, info = env.reset()
        done = False
        print(state.shape)

        while not done:
            if np.random.random() < epsilon:
                action = env.action_space.sample()  # Explore action space
            else:
                action = np.argmax(q_table[state])  # Exploit learned values

            next_state, reward, done, _, info = env.step(action)
            total_reward += reward
            old_value = q_table[state, action]
            next_max = np.max(q_table[next_state])

            # Update the Q-value
            new_value = (1 - alpha) * old_value + alpha * (reward + gamma * next_max)
            q_table[state, action] = new_value

            state = next_state

        rewards_per_episode.append(total_reward)
        if verbose:
            print(f"Episode {episode + 1}/{num_episodes}, Total Reward: {total_reward}")

    return q_table, rewards_per_episode

# Example usage
env = gym.make("gym_examples/Okey-v0", hand_length=5, points_goal=100, render_mode="text", lookup=False)
q_table, rewards = train_qlearning(env, 1000, 0.1, 0.99, 0.1, verbose=True)
