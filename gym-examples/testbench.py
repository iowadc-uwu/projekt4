import gym_examples
import gymnasium as gym
import numpy as np
from collections import defaultdict
import tqdm
from stable_baselines3 import SAC, DDPG, A2C, PPO
from stable_baselines3.common.callbacks import CheckpointCallback, CallbackList
from callback import PlotLearningCurveCallback


def run_custom_okey():
    env = gym.make("gym_examples/Okey-v0", hand_length=5, points_goal=100, render_mode="text", lookup=False)
    obs = env.reset()
    done = False
    
    max_steps = 2000
    steps = 0
    while not done and steps < max_steps:
        action = env.action_space.sample()
        obs, reward, done, _, info = env.step(action)
        
        steps += 1
    
    print(done, steps)
        

def make_env(expected_timesteps):
    env = gym.make("gym_examples/Okey-v0", hand_length=5, points_goal=100, render_mode="none", lookup=False)
    env = gym.wrappers.RecordEpisodeStatistics(env, deque_size=expected_timesteps*10)
    return env


def train_model(timesteps, model_name: str, discount_factor: float):
    checkpoint_callback = CheckpointCallback(
    save_freq=min(5000, timesteps),
    save_path="./projekt4/logs/" + model_name,
    name_prefix=model_name,
    save_replay_buffer=True,
    save_vecnormalize=True,
    )

    callback = CallbackList([checkpoint_callback, PlotLearningCurveCallback("./projekt4/logs/" + model_name)])

    env = make_env(timesteps)

    model = A2C('MultiInputPolicy', env, verbose=1, gamma=discount_factor)
    model.learn(total_timesteps=timesteps, callback=callback)

def continue_training(timesteps, model, model_name: str):
    checkpoint_callback = CheckpointCallback(
    save_freq=min(1000, timesteps),
    save_path="./projekt4/logs/" + model_name,
    name_prefix=model_name,
    save_replay_buffer=True,
    save_vecnormalize=True,
    )

    callback = CallbackList([checkpoint_callback])

    model.learn(total_timesteps=timesteps, callback=callback)

def test_model(n_episodes, model):
    env = make_env(n_episodes)

    for episode in tqdm.tqdm(range(n_episodes)):
        cum_reward = 0
        obs, info = env.reset()
        done = False

        while not done:
            action, _states = model.predict(obs, deterministic=True)
            obs, reward, terminated, truncated, info = env.step(action)
            done = terminated or truncated
            cum_reward += reward

        print(f"Cumulative reward: {cum_reward}")
    env.close()

def load_model(path, env):
    model = SAC.load(path, env)
    return model


timesteps = 100000
train_model(timesteps, "ddpg_gamma_0_99", 0.99)

# model1 = load_model("./projekt3/Humanoid/logs/rl_model_20000_steps.zip", make_env(timesteps))
# continue_learning(timesteps, model1, "gamma_0_99_continued")

# run_custom_okey()